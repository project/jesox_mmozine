<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php print $head ?>
<title><?php print $head_title ?></title>
<?php print $styles ?>
<?php print $scripts ?>
</head>
<body>
<!-- BEGIN wrapper -->
<div id="wrapper">
  <!-- BEGIN header -->
  <div id="header">
    <?php if ($logo): ?>
    <h1><a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>"><?php print check_plain($site_name); ?></a></h1>
    <?php endif; ?>
    <div class="ad"> <?php print $top_banner; ?> </div>
  </div>
  <!-- END header -->
  <!-- BEGIN body -->
  <div id="body">
    <div class="buffer">
      <!-- BEGIN subscribe -->
      <div class="subscribe"> <?php print $top_links_menu; ?> </div>
      <!-- END subscribe -->
      <!-- BEGIN pages -->
      <div class="pages">
      <?php print $primary_menu; ?>
      </div>
      <!-- END pages -->
      <!-- BEGIN categories -->
      <div class="categories">
      <?php print $secondary_menu; ?>
      </div>
      <!-- END categories -->
      <!-- BEGIN content -->
      <div id="content">
       <div class="latest">
        <?php print $featured_post; ?>
       </div>
       <?php print $content; ?>
      </div>
      <!-- END content -->
      <!-- BEGIN sidebar -->
      <div id="sidebar">
	<?php print $search_block ?>
	<div class="ads">
	<?php print $ads_right_block; ?>
	</div>
        <?php print $right_bar; ?>
      </div>
      <!-- END sidebar -->
      <div class="break"></div>
    </div>
  </div>
  <!-- END body -->
  <!-- BEGIN footer -->
  <div id="footer">
    <!-- begin recent posts -->
    <div class="box">
      <?php print $footer_left; ?>
    </div>
    <!-- end recent posts -->
    <!-- begin recent comments -->
    <div class="box">
      <?php print $footer; ?>
    </div>
    <!-- end recent comments -->
    <!-- begin about me -->
    <div class="about">
      <?php print $footer_right; ?>
    </div>
    <!-- end about me -->
    <!-- begin links -->
    <div class="links">
      <div class="l"><?php print $footer_links_left; ?></div>
      <div class="r"><?php print $footer_links_right; ?></div>
    </div>
    <!-- end links -->
  </div>
  <!-- END footer -->
</div>
<!-- END  -->
<?php print $closure;?>
</body>
</html>
